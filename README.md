# **telme**
#### CLI inline telegram bot notifications

## TL;DR
1. Get a bot token ( [BotFather](https://telegram.me/botfather) )
2. `$ npm install -g node-telme`
3. `$ telme --init`
4. Need help? `$ telme --help`

## How do i get a bot token?
 - Get a telegram bot token from the BotFather, It takes less than a minute
   - Just talk to [BotFather](https://telegram.me/botfather)  and follow a few simple steps. Once you've created a bot and received your authorization token, copy it for later 

## Configure `telme`

Simply run `$ telme --init` and follow 2 easy steps. You will need the bot token at this stage.
This will help you generate a `.telmeconfig` file in your home directory. You can always run `--init` again to override values or just edit the file yourself.

#### Config file structure
The config file `.telmeconfig` should be located in your home folder and contain a valid JSON.

Example config (`~/.telmeconfig`): 
```json
{
  "version": "0.1.0",
  "profiles": {
    "profile_name": {
      "chat_id": 000000,
      "bot_token": "<bot_token>",
      "task_message_template": "*Task:*\n\n```sh\n%cmd%\n```\nHas finished.\n*Errors*:\n %errors%"
    },
    ...
  }
}
```
> `task_message_template` allows the following optional placeholders `%cmd%`, `%errors%`. These will be replaced with the actual command and errors.

## Profiles

You can set multiple profiles, that will target different bots and/or different chats.
> You can use the same bot in all profiles if you like, But the target chat can be different

To initialize a new profile:
```shell
$ telme --init --profile <profile_name>
```

## Examples:
###### Simple message
```shell
$ telme -m "Message to send"
```

In the next example a message will be sent every time the user `user` logs in to a tty.
> Added the next lines at the bottom of `~/.profile` file
```shell
# Telme on login
telme -m "A new Login:\`\`\` user: $(whoami) | hostname: $(hostname) | remote ip $(who | cut -d'(' -f2 | cut -d')' -f1)\`\`\` Hope this is you\!"
```

###### Task message
Task messages are a simple way to receive a message from your bot once a command has been finished. It will also let you know if you had any errors.
```shell
$ telme docker build . -t my-image/build/that/takes/for/ever
```
In this example, once the docker build has finished you will receive a message.

As mentioned before - you can also specify a profile.

```shell
$ telme -p movie-club curl https://example.com/large/file/download.mp4
```
this will send the message to the `movie-club` profile chat. (By the `movie-club` bot)  

## Using as a `node_module`
> Typescript users will have definitions
```javascript
import Telme from 'node-telme' // OR const Telme = require('node-telme').default

const config = {
  chat_id: 'somechatid',
  bot_token: 'bot-token'
}

Telme.sendMessage(config, 'Hi there!').then(_=>{
  ...
}).catch(console.error);

// %cmd% and %errors% will be replaced buy actual values.
config.task_message_template = 'Task: %cmd% is done!. Errors: %errors%';
const options = {
  command: 'ls',
  args: ['-lah'] // If no args pass in an empty array
};

Telme.runTask(config, options).then(_=>{
  ...
}).catch(console.error);

```



