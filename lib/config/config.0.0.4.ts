import { IConfig, EConfigVersions } from './config';

export default class Config004 {
  static parse(conf: IConfig004): IConfig {
    return {
      version: EConfigVersions.V010,
      profiles: {
        default: {
          chat_id: conf.USER_ID,
          bot_token: conf.TOKEN,
          task_message_template: conf.DONE_MESSAGE
        }
      }
    }
  }
}

export interface IConfig004 {
  USER_ID: string;
  TOKEN: string;
  DONE_MESSAGE: string;
}