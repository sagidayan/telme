'use strict';
const TelegramBot = require('node-telegram-bot-api');
import { IMessageConfig } from '../config/config';
import InvalidBotOrChatConfig from '../errors/invalid_bot_chat_config.error';
import InvalidArgumentsError from '../errors/invalid_arguments.error';
export default class SendMessage {
  static async send(config: IMessageConfig, msg: string) {
    validate(config, msg);
    const bot = new TelegramBot(config.bot_token);
    try {
      await bot.sendMessage(config.chat_id, `${msg}`, { parse_mode: 'Markdown' });
      return true;
    } catch (e) {
      throw new InvalidBotOrChatConfig();
    }
  }
}

function validate(config: IMessageConfig, msg: string) {
  if (!config.bot_token || !config.chat_id) throw new InvalidArgumentsError(`Config object must have bot_token<string>, chat_id<string>`);
  if (typeof msg !== 'string') throw new InvalidArgumentsError(`message must be of type string`)
}