#!/usr/bin/env node
import Telme from './telme';
import { ArgParser, ERunMode, ISimpleMessageOptions, ITaskOptions, IInitProfileOptions } from './utils';
import Config, { ITaskConfig } from './config/config';
import Init from './flows/init'
const { version } = require('../package.json');

async function main() {
  const parsed = ArgParser.parse(process.argv);
  Config.cliInit();
  let config: ITaskConfig;
  let options: any;
  switch (parsed.mode) {
    case ERunMode.VERSION:
      console.log(`[${Config.APP_NAME}] version ${version}`);
      break;
    case ERunMode.HELP:
      printHelp();
      break;
    case ERunMode.INIT:
      await Init.init(parsed.mode_data);
      break;
    case ERunMode.SIMPLE_MESSAGE:
      config = await Config.getConfig(parsed.mode_data.profileName);
      options = parsed.mode_data as ISimpleMessageOptions;
      await Telme.sendMessage(config, options.message);
      break;
    case ERunMode.TASK_MESSAGE:
      config = await Config.getConfig(parsed.mode_data.profileName);
      options = parsed.mode_data as ITaskOptions;
      await Telme.runTask(config, options);
      break;
  }

  return true;
}

function printHelp() {
  const cliFlags = ArgParser.CLI_OPTIONS;
  const message =
    `${Config.APP_NAME} v${version} - A CLI Telegram message tool

[Usage]: $ ${Config.APP_NAME} <telme_options> <?command> <arguments>

Options:
\t ${cliFlags.versionFlags.join(', ')} \t\t Print ${Config.APP_NAME} version.
\t ${cliFlags.helpFlags.join(', ')} \t\t This help page.
\t ${cliFlags.profileFlags.join(', ')} \t\t Specify a profile to use. This is optional. defaults to 'default' profile.
\t ${cliFlags.initFlags.join(', ')} \t\t Will generate a config file for a given profile (defaults to 'default' profile).
\t ${cliFlags.messageFlags.join(', ')} \t\t Send a simple message.

Examples:
init:
\t '${Config.APP_NAME} --init' - init a default profile
\t '${Config.APP_NAME} -i -p <profile-name>' - init a 'named' profile 
tasks:
\t '${Config.APP_NAME} docker-compose pull' - Send a message to default profile once the command 'docker-compose pull' is done
\t '${Config.APP_NAME} -p <profile-name> docker-compose pull' - Send a message to <profile-name> profile once the command 'docker-compose pull' is done

messages: 
\t '${Config.APP_NAME} -m "text to send"' - Send a message to default profile
\t '${Config.APP_NAME} -p <profile-name> -m "text to send"' - Send message to <profile-name> profile

`;

  console.log(message);
}

main().then(_ => {
  process.exit(0);
}).catch(error => {
  const exitCode = error.exitCode || 1;
  console.error(`[${Config.APP_NAME}] ERROR: ${error.message}`);
  console.log(`[${Config.APP_NAME}] For help run '$ ${Config.APP_NAME} -h'`);
  process.exit(exitCode);
});