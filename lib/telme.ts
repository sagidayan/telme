process.env.NTBA_FIX_319 = 'junk';
import SendMessage from './flows/send_message';
import TaskMessage from './flows/task_message';
import { ITaskConfig as TaskConfig, IMessageConfig as MessageConfig } from './config/config';
import { ISimpleMessageOptions, ITaskOptions } from './utils';

export default class Telme {
  static async sendMessage(config: MessageConfig, msg: string) {
    await SendMessage.send(config, msg);
    return true;
  }
  static async runTask(config: TaskConfig, options: ITaskOptions) {
    await TaskMessage.run(config, options);
  }
}
export namespace Interfaces {
  export interface IMessageConfig extends MessageConfig { };
  export interface ITaskConfig extends TaskConfig { };
  export interface IMessageOptions extends ISimpleMessageOptions { };
  export interface ITaskMessageOptions extends ITaskOptions { };
}