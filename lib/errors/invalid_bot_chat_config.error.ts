import BaseError from './base_error';

export default class InvalidBotOrChatConfig extends BaseError {
  constructor() {
    super(`bot_token OR chat_id are invalid`, BaseError.ErrorCodes.INVALID_BOT_CHAT_CONFIG);
  }
}