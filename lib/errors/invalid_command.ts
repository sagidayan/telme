import BaseError from './base_error';

export default class InvalidCommandError extends BaseError {
  constructor(msg: string) {
    super(msg, BaseError.ErrorCodes.INVALID_COMMAND_ERROR);
  }
}