import BaseError from './base_error';

export default class InvalidArgumentsError extends BaseError {
  constructor(msg: string) {
    super(`Invalid Arguments: ${msg}`, BaseError.ErrorCodes.INVALID_ARGS_ERROR);
  }
}