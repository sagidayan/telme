import ErrorCodes from './error_codes';

export default class BaseError extends Error {
  readonly exitCode: ErrorCodes;
  static readonly ErrorCodes = ErrorCodes;
  constructor(message: string, code: ErrorCodes) {
    super(message);
    this.exitCode = code;
  }
}