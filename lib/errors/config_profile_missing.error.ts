import BaseError from './base_error';

export default class ConfigProfileError extends BaseError {
  constructor(msg: string) {
    super(msg, BaseError.ErrorCodes.CONFIG_PROFILE_ERROR);
  }
}