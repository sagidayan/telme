import BaseError from './base_error';

export default class ConfigFileMissingError extends BaseError {
  constructor(msg: string) {
    super(msg, BaseError.ErrorCodes.CONFIG_FILE_MISSING_ERROR);
  }
}