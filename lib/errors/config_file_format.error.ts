import BaseError from './base_error';

export default class ConfigFileFormatError extends BaseError {
  constructor(msg: string) {
    super(msg, BaseError.ErrorCodes.CONFIG_FILE_FORMAT_ERROR);
  }
}